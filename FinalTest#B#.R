library(ggplot2)
library(dplyr)
setwd("C:/Users/lclis/Desktop/�������/��� �/����� �/����� ��� ������/����")
onion.raw<-read.csv('OnionOrNot.csv')
onion<-onion.raw

#1b
str(onion)
YesNo<- function(label){
  if(label==1) return('yes')
  return('no')
}

onion$label<-sapply(onion$label,YesNo)  
onion$label<-as.factor(onion$label)

#1c
onion$text<- as.character(onion$text)
str(onion)
table(onion$label)
#1d
x<-c(length(onion$label[onion$label=='yes']),length(onion$label[onion$label=='no']))
labels<-c('YES','NO')

pct <- round(x/sum(x)*100)
lbls <- paste(labels, pct) # add percents to labels
lbls <- paste(lbls,"%",sep="") # ad % to labels
pie(x,labels = lbls, col=rainbow(2),main="Onion Or Not")

#2a
library(tm)
onion.corpus <- Corpus(VectorSource(onion$text))

clean.corpus <- tm_map(onion.corpus, removePunctuation)
clean.corpus <- tm_map(clean.corpus, removeNumbers)
clean.corpus <- tm_map(clean.corpus, content_transformer(tolower))
clean.corpus <- tm_map(clean.corpus, removeWords, stopwords())
clean.corpus <- tm_map(clean.corpus, stripWhitespace)

dtm <- DocumentTermMatrix(clean.corpus)
dim(dtm)

#2b
#n <- seq(100,1000,length = 181)
#dtm.freq <- DocumentTermMatrix(clean.corpus, list(dictionary = findFreqTerms(dtm,n))) 
#dim(dtm.freq)
#2c
dtm.freq <- DocumentTermMatrix(clean.corpus, list(dictionary = findFreqTerms(dtm,150))) 
dim(dtm.freq)
#2d

conv_01 <- function(x){
  x <- ifelse(x>0,1,0)
  return (as.integer(x))
}
dtm.final <- apply(dtm.freq, MARGIN = 1:2, conv_01)
onion.dtm.df <- as.data.frame(dtm.final)
onion.dtm.df$label <- onion$label
#2e
library(caTools)
library(ROSE)
filter <- sample.split(onion.dtm.df$label,SplitRatio = 0.7 )

onion.train <- subset(onion.dtm.df,filter==T)
onion.test <- subset(onion.dtm.df,filter==F)
dim(onion.dtm.df)
dim(onion.test)
dim(onion.train)

#3
onion.model <- glm(label ~., family = binomial(link = 'logit'), data = onion.train)
summary(onion.model)
prediction <- predict(onion.model, onion.test, type = 'response')
actual <- onion.test$label

confusion_matrix<- table(actual,prediction>0.5)

recall<- confusion_matrix[2,2]/(confusion_matrix[2,2]+confusion_matrix[2,1])
precision<- confusion_matrix[2,2]/(confusion_matrix[2,2]+confusion_matrix[1,2])
#3b
library(rpart)
library(rpart.plot)
model.dt <- rpart(label ~ ., onion.train)
rpart.plot(model.dt, box.palette = "RdBu", shadow.col = "gray", nn = TRUE)
predictiontr <- predict(model.dt, onion.test)
predict.prob.yes <- predictiontr[,'yes']
cf<-table(actual,predict.prob.yes>0.5)

recall<- cf[2,2]/(cf[2,2]+cf[2,1])
precision<- cf[2,2]/(cf[2,2]+cf[1,2])

#3d
library(pROC)

rocCurveLG <- roc(actual,prediction, direction = ">", levels = c("yes", "no"))
rocCurveDT <- roc(actual,predict.prob.yes, direction = ">", levels = c("yes", "no"))

plot(rocCurveLG, col = 'red',main = "ROC Chart")
par(new = TRUE)
plot(rocCurveDT, col = 'blue',main = "ROC Chart")

auc(rocCurveLG)
auc(rocCurveDT)
#4
dtm.freq <- DocumentTermMatrix(clean.corpus, list(dictionary = findFreqTerms(dtm,950))) 
dim(dtm.freq)

new.dtm <- apply(dtm.freq, MARGIN = 1:2, conv_01)
onion.dtm.new <- as.data.frame(new.dtm)
onion.dtm.new$label <- onion$label

filter <- sample.split(onion.dtm.new$label,SplitRatio = 0.7 )

onion.new.train <- subset(onion.dtm.new,filter==T)
onion.new.test <- subset(onion.dtm.new,filter==F)
dim(onion.dtm.new)
dim(onion.new.test)
dim(onion.new.train)

model.dt.new <- rpart(label ~ ., onion.new.train)
rpart.plot(model.dt.new, box.palette = "RdBu", shadow.col = "gray", nn = TRUE)

prediction.new<-predict(model.dt.new, onion.new.test)
prediction.yes<-prediction.new[,'yes']
actual<-onion.new.test$label
cf.new<-table(actual,prediction.yes)
accuracy<-cf.new[1,1]/dim(onion.dtm.new)[1] 
